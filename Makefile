PROG := alumar
BIN_DIR := ./bin
SRC_DIR := ./src
REL_DIR := ./release
BUILD_ARGS := --od ${BIN_DIR} -J
RUN_ARGS := -m1 -l2 -nlc -vi -dx -win 1546x1162

CMD := ${BIN_DIR}/${PROG}.cmd
BDS := ${BIN_DIR}/${PROG}.bds
CAS := ${BIN_DIR}/${PROG}.500.cas
WAV := ${BIN_DIR}/${PROG}.500.wav

${BIN_DIR}/%.cmd: ${SRC_DIR}/%.z
	zmac ${BUILD_ARGS} --oo cmd $<

${BIN_DIR}/%.bds: ${SRC_DIR}/%.z
	zmac ${BUILD_ARGS} --oo bds $<

${BIN_DIR}/%.500.cas: ${SRC_DIR}/%.z
	zmac ${BUILD_ARGS} --oo 500.cas $<

${BIN_DIR}/%.500.wav: ${SRC_DIR}/%.z
	zmac ${BUILD_ARGS} --oo 500.wav $<

clean:
	rm -rf ${BIN_DIR}
	rm -rf ${REL_DIR}

test: ${BDS}
	trs80gp ${RUN_ARGS} $<

release: ${CMD} ${CAS} ${WAV}
	mkdir -p ${REL_DIR}
	cp ${CMD} ${REL_DIR}
	cp ${CAS} ${REL_DIR}
	cp ${WAV} ${REL_DIR}
	cp LICENSE ${REL_DIR}
	cp MANUAL.txt ${REL_DIR}

wav: ${WAV}

default: ${CMD}