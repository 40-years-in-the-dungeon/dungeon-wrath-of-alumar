# Dungeon: Wrath of Alumar

*Dungeon: Wrath of Alumar* is a First-Person Dungeon Crawl game for the TRS-80 designed using era appropriate (1980) documentation and design limitations.
While assembled using a modern assembler and tested on an emulator with debugging capabilities, the intent is a period accurate experience.

*Dungeon: Wrath of Alumar* is part of my [40 Years in the Dungeon](https://gitlab.com/40-years-in-the-dungeon/40-years-in-the-dungeon) project, and the first game in the *Dungeon* series.

## Building

*Dungeon: Wrath of Alumar* was developed on Linux and assembled using [zmac](http://48k.ca/zmac.html).
Once `zmac` is installed and available on your `PATH`, running `make` will build the `.cmd` file, which is the most common file type for playing TRS-80 games on emulators.

If you wish to play around with the game code, and have [trs80gp](http://48k.ca/trs80gp.html) on your `PATH`, running `make test` will build the `.bds` file and immediately open the game in `trs80gp`, which provides a much nicer debugging experience.

## Running

For playing *Dungeon: Wrath of Alumar* on modern hardware, I recommend the [trs80gp](http://48k.ca/trs80gp.html) emulator.
You can jump straight into the game with `trs80gp alumar.cmd`.

If you want to play *Dungeon: Wrath of Alumar* on original hardware, the easiest method would be to run `make wav` which will produce a 500-baud `.wav` file which can be loaded from your phone via the cassette-in port on the TRS-80.
For more on using your phone to load software, you can try [this post on the Voidstar blog](https://voidstar.blog/tandy-radio-shack-computer-cassette-recorder-trs-ccr-usage/).